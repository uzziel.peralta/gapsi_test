
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';

import { http } from '../helpers/axios';
import { useState, useEffect } from 'react';
import { Button } from '@mui/material';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useFormSupplier } from './useFormSupplier';
import { useDeleteSupplier } from './useDeleteSupplier';

export const ViewSuppliers = () => {

  /* Define columns */
  const columns = [
    { id: 'index', label: '#', minWidth: 80, align: "center"},
    { id: 'name', label: 'Nombre', minWidth: 230 },
    { id: 'social_reason', label: 'Razón Social', minWidth: 300 },
    { id: 'address', label: 'Dirección', minWidth: 300 },
    { id: 'buttons', label: '', minWidth: 200 },
  ];

  /* Define states  */
  const [suppliers, setSuppliers] = useState([])
  const [total_rows, setTotalRows] = useState(0)
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(20)
  const [params, setParams] = useState({query: ""})

  /**
   * Load suppliers data from backend
   */
  const loadSuppliers = () => {

    http.get("suppliers/", params).then(({data}) => {
      setSuppliers(data.data);
      setTotalRows(data.total_rows)
    })

  }

  /* Import hook for supplier */
  const { ModalSupplier, openModalSupplier } = useFormSupplier(loadSuppliers);

  /* Import hook for delete supplier */
  const {DeleteModal, openDeleteModal} = useDeleteSupplier(loadSuppliers);

  const editSupplier = (row) => {
    console.log(row)
  }

  const deleteSupplier = (row) => {
    openDeleteModal(row);
  }

  /**
   * Open form to create supplier
   */

  const newSupplier = () => {
    openModalSupplier(null)
  }

  /* Run when start page */
  useEffect(() => {
    loadSuppliers();
  }, []);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    loadSuppliers();
  };

  return (
    <>
      <Paper sx={{ width: '100%', overflow: 'hidden' }}>
        <div className='d-flex justify-content-end'>
          <Button variant="contained" size="large" className='mx-5' onClick={newSupplier}>
            Nuevo
          </Button>
        </div>
        <TableContainer sx={{ maxHeight: 440 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {suppliers
                .map((row, i) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                      {
                        columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {
                              column.id == 'buttons' &&
                              <>
                                <div className="d-flex justify-content-between">
                                  <Button variant="contained" color="success" className="mx-1"  onClick={()=> editSupplier(row)} style={{display: "none"}}> 
                                    Editar
                                  </Button>
                                  <Button variant="contained" color="error" className="mx-1" onClick={()=> deleteSupplier(row)}>
                                    Borrar
                                  </Button>
                                </div>
                              </>
                            }
                            {
                              column.id != 'buttons' &&
                              <>
                                {column.id === "index" ? (i+1) : value}
                              </>
                            }
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          component="div"
          count={total_rows}
          rowsPerPageOptions={[20]}
          rowsPerPage={20}
          page={page}
          onPageChange={handleChangePage}
        />
      </Paper>
      <DeleteModal></DeleteModal>
      <ModalSupplier></ModalSupplier>
    </>
  );
}

