import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

import { useState } from 'react';
import { http } from '../helpers/axios';
import { Alert, Snackbar } from '@mui/material';

/**
 * Hook for managing confirm of delete supplier
 * @returns 
 */
export const useDeleteSupplier = (reloadSuppliers) => {

  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [supplier, setSupplier] = useState({});

  // Params for alert results
  const [message, setMessage] = useState('');
  const [result, setResult] = useState(true);
  const [showAlert, setShowAlert] = useState(false);


  const openDeleteModal = (supplier) => {
    setSupplier(supplier)
    setLoading(false);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const deleteSupplier = () => {
    setLoading(true);

    http.delete("suppliers/" + supplier.id).then(({data}) => {
      // Assign parameters
      setLoading(false)
      setMessage(data.message);
      setResult(data.rs);
      setShowAlert(true);
      // Close all and reload main view
      if(data.rs){
        setOpen(false);
        reloadSuppliers();
      }
    }).catch(({response}) => {
      // Assign parameters
      setLoading(false)
      setMessage(response.data.message);
      setResult(response.data.rs);
      setShowAlert(true);
    });
  }

  const closeAlert = () => {
    setShowAlert(false);
  }

  const DeleteModal = () => (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          ¿Seguro de eliminar el proveedor?
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Nombre: {supplier.name}<br/>
            Razón Social: {supplier.social_reason}<br/>
            Dirección: {supplier.address}<br/>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancelar</Button>
          <Button onClick={deleteSupplier} autoFocus disabled={loading}>
            {loading ? 'Eliminando' : 'Sí, eliminar'}
          </Button>
        </DialogActions>
      </Dialog>

      <Snackbar open={showAlert} autoHideDuration={1000} onClose={closeAlert}>
        <Alert severity={result ? 'success': 'error'} sx={{ width: '100%' }}>
          {message}
        </Alert>
      </Snackbar>
    </div>
  );

  return {
    DeleteModal,
    openDeleteModal
  }
}