import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';

import { useState } from 'react';
import { http } from '../helpers/axios';
import { Alert, Snackbar, TextField } from '@mui/material';

import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from "react-hook-form";
import * as yup from "yup";

/**
 * Hook for supplier form
 * @returns 
 */
export const useFormSupplier = (reloadSuppliers) => {

  /* Declare object from hook-form to validate and submit values */
  const { register, handleSubmit, reset, formState: { errors } } = useForm({
    defaultValues: {
      name: "", social_reason: "", address: ""
    },
    resolver: yupResolver(
      yup.object({
        name: yup.string().required().label('nombre'),
        social_reason: yup.string().required().label('razón social'),
        address: yup.string().label('dirección'),
      }).required()
    )
  });

  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);

  // Params for alert results
  const [message, setMessage] = useState('');
  const [result, setResult] = useState(true);
  const [showAlert, setShowAlert] = useState(false);

  const openModalSupplier = (supplier) => {
    if(supplier !== null)
      reset(supplier)
    else{
      reset({name: "", social_reason: "", address: ""})
    }
    setLoading(false);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const postSupplier = (data) => {

    setLoading(true);

    http.post("suppliers/", data).then(({data}) => {
      // Assign parameters
      setLoading(false)
      setMessage(data.message);
      setResult(data.rs);
      setShowAlert(true);
      // Close all and reload main view
      if(data.rs){
        setOpen(false);
        reloadSuppliers();
      }
    }).catch(({response}) => {
      // Assign parameters
      setLoading(false)
      setMessage(response.data.message);
      setResult(response.data.rs);
      setShowAlert(true);
    });
  }

  const closeAlert = () => {
    setShowAlert(false);
  }

  const ModalSupplier = () => (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        className='dialog-width-form'
        fullWidth={true}
      >
        <DialogTitle id="alert-dialog-title">
          Crear Nuevo Proveedor
        </DialogTitle>
        <DialogContent >

            <TextField
              autoFocus
              margin="dense"
              id="name" {...register("name")}
              label="Nombre"
              type="text"
              fullWidth
              variant="standard"
            />
            <p className='errors mt-2 w-100'>{errors.name?.message}</p>

            <TextField
              autoFocus
              margin="dense"
              id="social_reason"  {...register("social_reason")}
              label="Razón Social"
              type="text"
              fullWidth
              variant="standard"
            />
            <p className='errors mt-2 w-100'>{errors.social_reason?.message}</p>

            <TextField
              autoFocus
              margin="dense"
              id="address" {...register("address")}
              label="Dirección"
              type="text"
              fullWidth
              variant="standard"
            />
            <p className='errors mt-2 w-100'>{errors.address?.message}</p>

        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancelar</Button>
          <Button onClick={handleSubmit(postSupplier)} autoFocus disabled={loading}>
            {loading ? 'Guardando' : 'Guardar'}
          </Button>
        </DialogActions>
      </Dialog>

      <Snackbar open={showAlert} autoHideDuration={1000} onClose={closeAlert}>
        <Alert severity={result ? 'success': 'error'} sx={{ width: '100%' }}>
          {message}
        </Alert>
      </Snackbar>
    </div>
  );

  return {
    ModalSupplier,
    openModalSupplier
  }
}