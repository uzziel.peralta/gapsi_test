import React from 'react'

export const MainHeader = () => {
  return (
    <div id="main-header" className='mb-5 pb-3 pt-5'>
      <div className="d-flex justify-content-between align-items-center px-4">
        <h3>e-Commerce Gapsi</h3>
          <img src="/logo.png" className='logo-header'></img>
      </div>
    </div>
  )
}
