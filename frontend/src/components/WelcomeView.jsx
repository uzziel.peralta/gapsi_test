import { Dialog, DialogActions, DialogContent, DialogTitle } from "@mui/material"
import { useState, useEffect } from "react"
import { Button } from '@mui/material';
import { http } from "../helpers/axios";

export const WelcomeView = () => {

  const [open, setOpen] = useState(true)
  const [version, setVersion] = useState("")
  const [user, serUser] = useState("")

  const continue_app = () => {
    setOpen(false);
  }

  const loadVersion = () => {

    http.get("auth/").then(({data}) => {
      setVersion(data.version)
      serUser(data.user_name)
    })

  }

  useEffect(() => {
    loadVersion();
  }, []);

  return (
    <>
      <Dialog
        fullWidth={true}
        open={open}
      >
        <DialogTitle style={{background: "#F2F2F2"}} className="px-3">e-Commerce Gapsi</DialogTitle>
        <DialogContent>
          <div className="d-flex justify-content-center mb-5 mt-5">
            <img src="/logo.png"></img>
          </div>
          <div className="d-flex justify-content-center mb-5">
            <h4>Bienvenido: {user}</h4>
          </div>
          <div className="d-flex justify-content-center mb-5">
            <Button variant="contained" size="large" className='mx-5' onClick={continue_app}>
              Continuar
            </Button>
          </div>
        </DialogContent>
        <DialogActions style={{background: "#F2F2F2"}} className="d-flex justify-content-end p-2">
          Versión: {version}
        </DialogActions>
      </Dialog>
    </>
  )
}
