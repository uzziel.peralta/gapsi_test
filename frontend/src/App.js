import { Card, Container } from '@mui/material';
import './assets/styles/index.scss';
import { MainHeader } from './components/MainHeader';
import { ViewSuppliers } from './components/ViewSuppliers';
import { WelcomeView } from './components/WelcomeView';

const App = () =>{
  return (
    <Container id="layout">
      <Card>
        <MainHeader/>
        <ViewSuppliers ></ViewSuppliers>
        <WelcomeView></WelcomeView>
      </Card>
    </Container>
  );
}

export default App;
