import axios from "axios";

const baseUrl = process.env.REACT_APP_BASE_URL;

/**
 * Create axios object with token header
 * @returns 
 */
const CreateAxios = () => {
  // Return axios object
  let headers = {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin' : '*'
  };
  return axios.create({
    baseURL: baseUrl,
    headers
  });
 }

export let http = CreateAxios();
