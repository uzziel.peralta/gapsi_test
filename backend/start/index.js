require('dotenv').config();

// Declare backend application
const Koa = require('koa');
const router = require('koa-router');
const cors = require("@koa/cors");

// Create koa app
const app =  new Koa();
app.port = (process.env.PORT || 3000)
app.use(cors());

// Import urls for modules
let auth = require('../routes/auth.js');
app.use(auth.routes());

let suppliers = require('../routes/suppliers.js');
app.use(suppliers.routes());

// Run NPM Server
app.listen(app.port, ()=>{
  console.log(`Server listening on port ${app.port}`);
});