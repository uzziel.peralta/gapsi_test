
const Router = require('koa-router');
const router = Router({ prefix: '/auth' });


/**
 * @api GET /auth
 * Return application version and user id
 */
router.get('/', (ctx, next) => {
	ctx.body = {
      "user_id": "01",
      "user_name": "Candidato 01",
      "version": process.env.APP_ENV
  };
});


module.exports = router;
